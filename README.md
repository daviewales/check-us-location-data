# Check US Location Data

Scripts to check US location data based on known attributes.
For example, check whether this location (latitude, longitude) is within a given state.

Developed to cross-check data from the [PDAP Project](https://www.dolthub.com/repositories/pdap/datasets) on DoltHu# Check US Location Data

Scripts to check US location data based on known attributes.
For example, check whether this location (latitude, longitude) is within a given state.

Developed to cross-check data from the [PDAP Project](https://www.dolthub.com/repositories/pdap/datasets) on DoltHub

## Dependencies

Just run `pip install -r requirements.txt`

## Usage

   python3 ./check_location_data.py data_source.csv us_location_data.gdb

The script expects the data source CSV to contain the following columns:

- state_iso
- lat
- lng

where `state_iso` is the short code for the state, and `lat`, `lng` are the latitude and longitude respectively.
This matches the format of the `agencies` dataset from PDAP.

The location data file can be any format which is readable by `geopandas` (which means any format `fiona` can read).
It is expected to contain a `STATE_ABBR` field (short code for state) and a geometry field.

This repository contains a copy of the 
[1:1,000,000-Scale State Boundaries of the United States](https://www.sciencebase.gov/catalog/item/581d0554e4b08da350d52784)
which is a GDB dataset which meets these requirements.
