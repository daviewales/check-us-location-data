#!/usr/bin/env python3

# Check Location Data
# Checks that the latitude and longitude in the source file match the specified state

import geopandas as gpd
from shapely.geometry import Point
import pandas as pd

LOCATION_DATA = 'statesp010g.gdb_nt00937/statesp010g.gdb'
SOURCE = 'agencies.csv'


def main():
    state_data = gpd.read_file(LOCATION_DATA).to_crs(epsg=4326)
    source = pd.read_csv(SOURCE)
    source_gdf = gpd.GeoDataFrame(source, geometry=gpd.points_from_xy(source['lng'], source['lat']))
    source_gdf.rename_geometry('source_points', inplace=True)
    source_gdf.set_crs(epsg=4326, inplace=True)

    merged_gdf = state_data.merge(source, how='left', left_on='STATE_ABBR',  right_on='state_iso')
    print(merged_gdf.columns)
    test = merged_gdf.loc[~merged_gdf['geometry'].contains(merged_gdf['source_points'])]

    print(test)


if __name__ == '__main__':
    main()
